var gulp = require('gulp'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify');

gulp.task('scripts', function() {
  return gulp.src('./src/directives.js')
    .pipe(concat('directives.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist/'));
});

// Default Task
gulp.task('default', ['scripts']);
