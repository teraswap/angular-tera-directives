(function() {
  "use strict";

  var boxFilter = [function() {
    return {
      restrict: 'A',
      templateUrl: 'angular-tera-directives/box/box-filter-directive.html',
      link: function($scope, $element, $attr) {
        var length = Object.keys($scope.filter).length;

        Object.keys($scope.filter).forEach(function(item) {

          if (typeof $scope.filter[item] == 'boolean') {
            length--;
          } else if (typeof $scope.filter[item] == 'object' && Object.keys($scope.filter[item]).length > 1) {
            length++;
          }
        });

        $scope.division = Math.floor(10 / length);
        $scope.placeholders = [];

        Object.keys($scope.filter).forEach(function(item) {
          if (typeof $scope.filter[item] != 'boolean') {
            $scope.placeholders[item] = $attr.module.toUpperCase() + '.' + item.toUpperCase();
          }
        });
      }
    };
  }];

  var boxHeader = ['$state', function($state) {
    return {
      restrict: 'A',
      templateUrl: 'angular-tera-directives/box/box-header-directive.html',
      scope: true,
      link: function($scope, $element, $attr) {

        if (angular.isDefined($attr.titleLegend) && angular.isDefined($attr.titleIcon)) {

          $scope.titleLegend = $attr.titleLegend;
          $scope.titleIcon = $attr.titleIcon;

          $scope.actions = [];
          $scope.icons = [];

          if (angular.isDefined($attr.actions) && angular.isDefined($attr.icons)) {
            var icons = $attr.icons.split(',');
            $attr.actions.split(',').forEach(function(action, index) {
              $scope.actions.push(action.trim());
              $scope.icons.push(icons[index]);
            });
          }
        } else {
          console.error('box-header-directive: you must specify the title data [data-title-legend, data-title-icon]');
        }
      }
    };
  }];

  var boxHeaderEvaluateAction = [function() {
    return {
      restrict: 'A',
      link: function($scope, $element, $attr) {
        var isLink = $attr.boxHeaderEvaluateAction.charAt(0) === "#";
        $element.attr('href', isLink ? $attr.boxHeaderEvaluateAction : '#');

        if (!isLink) {
          $element.on('click', function() {
            $scope.$emit($attr.boxHeaderEvaluateAction);
          });
        }

      }
    };
  }];

  var boxSimpleCollection = [function() {
    return {
      restrict: 'A',
      link: function($scope, $element, $attr) {
        if (typeof $attr.boxSimpleCollection !== undefined) {

          Object.keys($scope.filter).forEach(function(item) {
            if (typeof $scope.filter[item] == 'object') {
              $scope.key = item;
            }
          });

          $scope.$watch($attr.boxSimpleCollection, function(populated) {
            if (populated) {
              $scope.simpleCollection = populated;
            }
          });
        }
      }
    };
  }];

  var isAnAddAction = [function() {
    return {
      restrict: 'A',
      link: function($scope, $element, $attr) {
        if ($attr.isAnAddAction.endsWith('new')) {
          $element.show();
        } else {
          $element.hide();
        }
      }
    };
  }];

  var boxNotBooleanShow = [function() {
    return {
      restrict: 'A',
      link: function($scope, $element, $attr) {
        if ($attr.boxNotBooleanShow === 'true' || ($attr.boxNotBooleanShow !== 'false') && $attr.boxNotBooleanShow || false) {
          $element.hide();
        } else {
          $element.show();
        }
      }
    };
  }];

  var tableHead = ['$rootScope', function($rootScope) {
    return {
      restrict: 'A',
      scope: true,
      templateUrl: 'angular-tera-directives/table/table-head-directive.html',
      link: function($scope, $element, $attr) {

        if (angular.isDefined($attr.module)) {

          $scope.labels = [];
          $scope.headFields = [];
          $scope.sortFields = [];
          $scope.responsiveFields = [];
          $scope.haveActions = false;

          $scope.module = $attr.module;

          $scope.needSort = function(field) {
            return $scope.sortFields.indexOf(field) >= 0;
          };

          if (angular.isDefined($attr.sortBy)) {
            $attr.sortBy.split(',').forEach(function(field) {
              $scope.sortFields.push(field.trim());
            });
          }

          if (angular.isDefined($attr.sortMethod)) {
            $scope.sortMethod = $attr.sortMethod;
          }

          if (angular.isDefined($attr.pagerName)) {
            $scope.$watch($attr.pagerName, function(pagerPopulated) {
              if (pagerPopulated) {
                $scope.pager = pagerPopulated;
              }
            });
          }

          $attr.fields.split(',').forEach(function(field) {
            $scope.headFields.push(field.trim());
            $scope.labels[field.trim()] = $attr.module.toUpperCase() + '.' + field.trim().toUpperCase();
          });

          if (angular.isDefined($attr.responsiveFields)) {
            $attr.responsiveFields.split(',').forEach(function(field) {
              $scope.responsiveFields.push(field.trim());
            });
          }

          if (angular.isDefined($attr.headClass)) {
            $scope.headClass = $attr.headClass;
          }

          $rootScope.$on('table-head:haveActions', function() {
            $scope.haveActions = true;
          });

          $rootScope.$on('table-head:noHaveActions', function() {
            $scope.haveActions = false;
          });

        } else {
          console.error('table-head-directive: you must specify a module name [data-module]');
        }

      }
    };
  }];

  var tableSortField = [function() {
    return {
      restrict: 'A',
      scope: true,
      link: function($scope, $element, $attr) {
        if ($scope.sortFields.indexOf($attr.tableSortField) >= 0) {
          $element.show();
          $element.on('click', function() {
            $scope.$apply(function() {
              $scope.$parent[$scope.sortMethod]($attr.tableSortField);
            });
          });
        } else {
          $element.hide();
        }
      }
    };
  }];

  var tableBody = ['$rootScope', function($rootScope) {
    return {
      restrict: 'A',
      scope: true,
      templateUrl: 'angular-tera-directives/table/table-body-directive.html',
      link: function($scope, $element, $attr) {

        $scope.isComplex = function(field) {
          return field.indexOf('.') >= 0;
        };

        $scope.processIfComplex = function(field) {

          if ($scope.isComplex(field)) {
            $scope.complex[field] = [];

            var keys = field.split('.');

            $scope.collection.forEach(function(item, index) {

              if (angular.isDefined(item[keys[0]]) && item[keys[0]] !== null) {

                var complexItem = item[keys[0]][keys[1]];

                complexItem = keys.length > 2 ? complexItem[keys[2]] : complexItem;
                if ($scope.complex[field].indexOf(complexItem) < 0) {
                  $scope.complex[field].push(complexItem);
                }
              } else {
                console.error('table-body-directive: null or undefined complex value [' + keys.toString().replace(',', '.') + ']');
              }
            });
          }
        };

        $scope.evaluateComposite = function(field) {
          var compositeValue = {
            isComposite: field.trim().indexOf(' ') >= 0 || field.trim().indexOf(':') >= 0 || field.trim().indexOf('/') >= 0 || field.trim().indexOf('-') >= 0
          };

          if (field.trim().indexOf(' ') >= 0) {
            compositeValue.compositeCharacter = ' ';
          } else if (field.trim().indexOf(':') >= 0) {
            compositeValue.compositeCharacter = ':';
          } else if (field.trim().indexOf('/') >= 0) {
            compositeValue.compositeCharacter = '/';
          } else if (field.trim().indexOf('-') >= 0) {
            compositeValue.compositeCharacter = '-';
          }

          return compositeValue;
        };

        $scope.$watchCollection($attr.collection, function(populated) {
          if (populated) {

            $scope.collection = populated;
            $scope.complex = {};

            var fields = [];
            $attr.fields.split(',').forEach(function(field) {
              fields.push(field.trim());

              var compositeResult = $scope.evaluateComposite(field.trim());
              if (compositeResult.isComposite) {
                field.trim().split(compositeResult.compositeCharacter).forEach(function(compositeField) {
                  $scope.processIfComplex(compositeField.trim());
                });
              } else if (field.trim().indexOf('|') >= 0) {
                field.split('|').forEach(function(filteredField) {
                  $scope.processIfComplex(filteredField[0].trim());
                });
              } else {
                $scope.processIfComplex(field.trim());
              }
            });

            $scope.actions = [];
            if (angular.isDefined($attr.bodyActionShow)) {
              $scope.actions.push('show');
              $rootScope.$emit('table-head:haveActions');
            }

            if (angular.isDefined($attr.bodyActionEdit)) {
              $scope.actions.push('edit');
              if ($attr.bodyActionEdit.length > 0) {
                $scope.eventEdit = $attr.bodyActionEdit;

              }
              $rootScope.$emit('table-head:haveActions');
            }

            if (angular.isDefined($attr.bodyActionEnableDisable)) {
              $scope.actions.push('enable-disable');
              $rootScope.$emit('table-head:haveActions');
            }

            if (angular.isDefined($attr.bodyActionRemove)) {
              $scope.actions.push('remove');
              $scope.eventRemove = $attr.bodyActionRemove;
              $rootScope.$emit('table-head:haveActions');
            }

            if (angular.isDefined($attr.dateFormat)) { // globals filters
              $scope.dateFormat = $attr.dateFormat;
            }

            if (angular.isDefined($attr.customActions)) {
              $scope.haveCustomActions = true;
              $rootScope.$emit('table-head:haveActions');
            }

            if (angular.isDefined($attr.separator)) {
              $scope.separator = $attr.separator;
            }

            if (angular.isDefined($attr.module)) {
              $scope.module = $attr.module;
            } else {
              console.error('table-body-directive: you must specify a module name [data-module]');
            }

            $scope.colspan = fields.length;
            $scope.bodyFields = fields;
          }
        });
      }
    };
  }];

  var isAuthorizedAction = ['$rootScope', function($rootScope) {
    return {
      restrict: 'A',
      link: function($scope, $element, $attr) {

        var show = false;

        if (typeof $rootScope.loggedUser != 'undefined') {
          $rootScope.loggedUser.authorities.forEach(function(role) {
            show |= role.authority === $attr.isAuthorizedAction;
          });
        }

        if (show) {
          $element.show();
        } else {
          $element.remove();
          $rootScope.$emit('table-head:noHaveActions');
        }
      }
    };
  }];

  var tableBodyHasAction = [function() {
    return {
      restrict: 'A',
      scope: true,
      link: function($scope, $element, $attr) {
        $attr.$observe('tableBodyHasAction', function() {
          $scope.$watch('actions', function(populated) {
            if (populated) {
              if (populated.indexOf($attr.tableBodyHasAction) >= 0) {
                $element.show();
              } else {
                $element.remove();
              }
            }
          });
        });
      }
    };
  }];

  var tableBodyActionEditItem = ['$window', function($window) {
    return {
      restrict: 'A',
      scope: true,
      link: function($scope, $element, $attr) {
        $attr.$observe('tableBodyActionEditItem', function(populated) {
          if (populated) {
            $element.on('click', function() {
              if (angular.isDefined($scope.eventEdit)) {
                $scope[$scope.eventEdit](JSON.parse(populated));
              } else {
                $window.location = '#/' + $scope.module + '/edit/' + JSON.parse(populated).id;
              }
            });
          }
        });
      }
    };
  }];

  var tableBodyActionRemoveItem = [function() {
    return {
      restrict: 'A',
      scope: true,
      link: function($scope, $element, $attr) {
        $attr.$observe('tableBodyActionRemoveItem', function(populated) {
          if (populated) {
            $element.on('click', function() {
              $scope[$scope.eventRemove](JSON.parse(populated));
            });
          }
        });
      }
    };
  }];

  var tableBodyValue = ['$filter', function($filter) {
    return {
      restrict: 'A',
      scope: true,
      link: function($scope, $element, $attr) {

        $scope.evaluateComposite = function(field) {
          var compositeValue = {
            isComposite: field.trim().indexOf(' ') >= 0 || field.trim().indexOf(':') >= 0 || field.trim().indexOf('/') >= 0 || field.trim().indexOf('-') >= 0
          };

          if (field.trim().indexOf(' ') >= 0) {
            compositeValue.compositeCharacter = ' ';
          } else if (field.trim().indexOf(':') >= 0) {
            compositeValue.compositeCharacter = ':';
          } else if (field.trim().indexOf('/') >= 0) {
            compositeValue.compositeCharacter = '/';
          } else if (field.trim().indexOf('-') >= 0) {
            compositeValue.compositeCharacter = '-';
          }

          return compositeValue;
        };

        $scope.isDate = function(field) {
          return (angular.isDefined(field) && ((field.toString().length === 13 && parseInt(field) > 0) || (field.toString().length === 10 && field.toString().indexOf('-') >= 0)));
        };

        var jsonObj = JSON.parse($attr.tableBodyValue),
          keys;

        var compositeResult = $scope.evaluateComposite(jsonObj[0]);

        if (compositeResult.isComposite) { // Evaluate if it is a composite field
          var compositeValue = [];
          jsonObj[0].split(compositeResult.compositeCharacter).forEach(function(field) {

            keys = field.split('.');
            var complexItem = jsonObj[1][keys[0]][keys[1]];

            complexItem = keys.length > 2 ? complexItem[keys[2]] : complexItem;
            compositeValue.push($scope.isComplex(field) ? complexItem : jsonObj[1][field]);
          });
          $element.html(compositeValue.join(compositeResult.isComposite ? ' ' + compositeResult.compositeCharacter + ' ' : ' '));

        } else if (jsonObj[0].indexOf('|') >= 0) { // Evaluate if it is a filtered field

          keys = jsonObj[0].split('|');
          $element.html($filter(keys[1])(jsonObj[1][keys[0]]));

          if (keys.length === 3 && keys[2] === 'color') {
            $element.addClass(jsonObj[1][keys[0]] >= 0 ? 'text-success' : 'text-danger');
          }

        } else {

          var field;

          if ($scope.isComplex(jsonObj[0])) {
            keys = jsonObj[0].split('.');
            if (angular.isDefined(jsonObj[1][keys[0]]) && jsonObj[1][keys[0]] !== null) {
              field = jsonObj[1][keys[0]][keys[1]];
            }

            field = keys.length > 2 ? field[keys[2]] : field;
          } else {
            field = jsonObj[1][jsonObj[0]];
          }

          // Evaluate if it is an image url
          if (angular.isDefined(field) && (new RegExp(/\.(jpeg|jpg|gif|png)/)).test(field.toString())) {
            $element.html($('<img/>').attr('src', field).addClass('img-circle'));
            $element.before().addClass('text-center').addClass('hidden-xs').addClass('image');
            // Evaluate if it is a timestamp
          } else if ($scope.isDate(field)) {
            var date = new Date(angular.isNumber(field) ? parseInt(field) : field);
            $element.html($filter('date')(date, $scope.dateFormat));
          } else {
            $element.html(field);
          }
        }
      }
    };
  }];

  var tableBodyActions = [function() {
    return {
      restrict: 'A',
      scope: true,
      link: function($scope, $element, $attr) {
        $scope.getContentUrl = function() {
          if (angular.isDefined($scope.haveCustomActions)) {
            return './app/' + $scope.module + '/views/custom-actions.html';
          } else if (angular.isDefined($scope.actions) && $scope.actions.length > 0) {
            return 'angular-tera-directives/table/table-actions-directive.html';
          } else {
            $element.hide();
          }
        };
      },
      template: '<div ng-include="getContentUrl()"></div>'
    };
  }];

  var tablePager = [function() {
    return {
      restrict: 'A',
      scope: true,
      templateUrl: 'angular-tera-directives/table/table-pager-directive.html',
      link: function($scope, $element, $attr) {
        $scope.$watchCollection($attr.collection, function(populated) {
          if (populated) {
            if (populated.length === 0) {
              $element.hide();
            } else {
              $element.show();
              if (angular.isDefined($attr.pagerName)) {
                $scope.$watch($attr.pagerName, function(pagerPopulated) {
                  if (pagerPopulated) {
                    $scope.pager = pagerPopulated;
                  }
                });
              }
            }
          }
        });

        $scope.pageChanged = function() {
          if (angular.isDefined($attr.changeMethod)) {
            $scope.$parent[$attr.changeMethod]();
          } else {
            $scope.search();
          }
        };
      }
    };
  }];

  var poweredBy = ['$window', function($window) {
    return {
      restrict: 'A',
      replace: true,
      scope: true,
      templateUrl: 'angular-tera-directives/miscellaneous/powered-by-directive.html',
      link: function($scope, $element, $attr) {
        $scope.color = angular.isDefined($attr.colorBlack) ? 'black' : 'white';

        var w = angular.element($window);
        $scope.getWindowHeight = function() {
          return w.height();
        };

        $scope.$watch($scope.getWindowHeight, function(populated) {
          if (populated) {
            var minMenuHeight = angular.element('.sidebar-menu').height() + angular.element('.user-panel').height() + angular.element('.main-header').height() + 100;

            if (populated > minMenuHeight) {
              $element.css({
                'position': 'absolute',
                'bottom': '0',
                'text-align': 'center',
                'padding': '1em'
              });

              $element.find('img').css('width', '70%');

              $element.show();
            } else {
              $element.hide();
            }
          }
        });

        w.bind('resize', function() {
          $scope.$apply();
        });
      }
    };
  }];

  var mainFooter = [function() {
    return {
      restrict: 'A',
      scope: true,
      templateUrl: 'angular-tera-directives/miscellaneous/footer-directive.html',
      link: function($scope, $element, $attr) {
        $element.css({
          'height': '3.5em',
          'padding': '11px'
        });

        if (angular.isDefined($attr.version)) {
          $scope.version = $attr.version;
        }

        if (angular.isDefined($attr.noLogo)) {
          $scope.noLogo = true;
        } else {
          $scope.color = angular.isDefined($attr.colorWhite) ? 'white' : 'black';
        }
      }
    };
  }];

  angular.module('angular-tera-directives/box/box-filter-directive.html', []).run(['$templateCache', function($templateCache) {
    $templateCache.put('angular-tera-directives/box/box-filter-directive.html',
      '<div class="row">' +
      '<div class="col-md-12">' +
      '<div class="box">' +
      '<div class="box-header">' +
      '<i class="fa fa-filter"></i>' +
      '<h3 class="box-title" data-translate="GLOBAL.FILTER"></h3>' +
      '<div class="box-tools pull-right">' +
      '<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="{{ \'GLOBAL.COLLAPSE\' | translate }}">' +
      '<i class="fa fa-minus"></i>' +
      '</button>' +
      '</div>' +
      '</div>' +
      '<div class="box-body">' +
      '<div class="row">' +
      '<div class="col-md-12">' +
      '<form role="form">' +
      '<div class="col-md-{{ division }}" data-ng-show="simpleCollection">' +
      '<select name="simpleCollection" class="form-control" data-ng-model="filter[key].id" data-ng-options="item.id as item.name for item in simpleCollection">' +
      '<option value="" data-translate="GLOBAL.FORM.SELECT_ONE"></option>' +
      '</select>' +
      '</div>' +
      '<div class="col-md-{{ division }}" data-ng-repeat="(key, value) in filter" data-box-not-boolean-show="{{ value }}">' +
      '<input type="text" placeholder="{{ placeholders[key] | translate }}" data-ng-model="filter[key]" class="form-control"/>' +
      '</div>' +
      '<div class="col-md-2 text-right pull-right">' +
      '<div class="btn-group">' +
      '<button class="btn btn-primary " data-ng-click="search()" title="{{ \'GLOBAL.SEARCH\' | translate}}" data-original-title="{{ \'GLOBAL.SEARCH\' | translate}}"><i class="fa fa-search" /></i></button>' +
      '<button class="btn btn-primary" data-ng-click="reset()" title="{{ \'GLOBAL.RESET\' | translate}}" data-original-title="{{ \'GLOBAL.RESET\' | translate}}"><i class="fa fa-undo" /></i></button>' +
      '</div>' +
      '</div>' +
      '</form>' +
      '</div>' +
      '</div>' +
      '</div>' +
      '</div>' +
      '</div>' +
      '</div>'
    );
  }]);

  angular.module('angular-tera-directives/box/box-header-directive.html', []).run(['$templateCache', function($templateCache) {
    $templateCache.put('angular-tera-directives/box/box-header-directive.html',
      '<div class="box-header with-border">' +
      '<i class="fa {{ titleIcon }}"></i>' +
      '<h3 class="box-title">{{ (titleLegend | uppercase) | translate }}</h3>' +
      '<div data-ng-show="actions && actions.length > 0" class="box-tools pull-right {{ actions.length > 1 ? \'btn-group\' : \'\' }}">' +
      '<a data-ng-repeat="(index, action) in actions" data-box-header-evaluate-action="{{ action }}" class="btn btn-primary btn-sm white-btn" data-toggle="tooltip">' +
      '<i class="fa {{ icons[index] }}"></i> <i class="ion ion-ios-plus-empty" data-is-an-add-action="{{ action }}"></i>' +
      '</a>' +
      '</div>' +
      '</div>'
    );
  }]);

  angular.module('angular-tera-directives/table/table-head-directive.html', []).run(['$templateCache', function($templateCache) {
    $templateCache.put('angular-tera-directives/table/table-head-directive.html',
      '<tr role="row">' +
      '<th class="{{ headClass }}" role="columnheader" rowspan="1" colspan="1" data-ng-repeat="field in headFields" data-ng-class="{true: \'hidden-xs\', false: \'\'}[responsiveFields.indexOf(field) >= 0]">' +
      '<span data-ng-hide="needSort(field)">{{ labels[field] | translate }}</span>' +
      '<a data-table-sort-field="{{ field }}">' +
      '{{ labels[field] | translate }} ' +
      '<span data-ng-show="pager.sort.field == field && pager.sort.direction == 0" class="fa fa-caret-down"></span>' +
      '<span data-ng-show="pager.sort.field == field && pager.sort.direction == 1" class="fa fa-caret-up"></span>' +
      '</a>' +
      '</th>' +
      '<th class="{{ headClass }} text-center" role="columnheader" rowspan="1" colspan="1" aria-label="{{ \'GLOBAL.ACTIONS\' | translate }}" data-translate="GLOBAL.ACTIONS" data-ng-show="haveActions"></th>' +
      '</tr>'
    );
  }]);

  angular.module('angular-tera-directives/table/table-body-directive.html', []).run(['$templateCache', function($templateCache) {
    $templateCache.put('angular-tera-directives/table/table-body-directive.html',
      '<tr data-ng-repeat="(index, item) in collection">' +
      '<td data-ng-repeat="field in bodyFields" data-table-body-value="{{ [field, item] }}"></td>' +
      '<td class="text-center" data-table-body-actions></td>' +
      '</tr>' +
      '<tr data-ng-show="collection == null || collection.length == 0">' +
      '<td colspan="{{ colspan + 1 }}">{{ \'GLOBAL.NOT_FOUND\' | translate }}</td>' +
      '</tr>'
    );
  }]);

  angular.module('angular-tera-directives/table/table-pager-directive.html', []).run(['$templateCache', function($templateCache) {
    $templateCache.put('angular-tera-directives/table/table-pager-directive.html',
      '<div class="box-footer">' +
      '<div class="row">' +
      '<div class="col-xs-6 hidden-xs">' +
      '<div class="dataTables_info">' +
      '{{ \'GLOBAL.PAGINATION.SHOWING.FROM\' | translate }} {{ pager.startItem }} {{ \'GLOBAL.PAGINATION.SHOWING.TO\' | translate }} {{ pager.finishItem }} {{ \'GLOBAL.PAGINATION.SHOWING.OF\' | translate }} {{ pager.totalItems }} {{ \'GLOBAL.PAGINATION.SHOWING.RESULTS\' | translate }}' +
      '</div>' +
      '</div>' +
      '<div class="col-md-6 col-sm-6 col-xs-12">' +
      '<div class="dataTables_paginate paging_bootstrap">' +
      '<pagination data-total-items="pager.totalItems" data-ng-model="pager.currentPage" data-max-size="pager.maxSize" data-num-pages="pager.numPages" data-ng-change="pageChanged()" data-previous-text="{{ \'GLOBAL.PAGINATION.PREVIOUS\' | translate }}" data-next-text="{{ \'GLOBAL.PAGINATION.NEXT\' | translate }}" data-first-text="{{ \'GLOBAL.PAGINATION.FIRST\' | translate }}" data-last-text="{{ \'GLOBAL.PAGINATION.LAST\' | translate }}"></pagination>' +
      '</div>' +
      '</div>' +
      '</div>' +
      '</div>'
    );
  }]);

  angular.module('angular-tera-directives/table/table-actions-directive.html', []).run(['$templateCache', function($templateCache) {
    $templateCache.put('angular-tera-directives/table/table-actions-directive.html',
      '<div class="btn-group hidden-xs">' +
      '<a class="btn btn-primary" data-ng-href="#/{{ module }}/show/{{ item.id }}" data-is-authorized-action="ROLE_ADMIN" data-table-body-has-action="show" title="{{ \'GLOBAL.SHOW\' | translate }}"><i class="fa fa-search-plus"></i></a>' +
      '<a class="btn btn-primary" data-is-authorized-action="ROLE_ADMIN" data-table-body-has-action="edit" data-table-body-action-edit-item="{{ item }}" title="{{ \'GLOBAL.EDIT\' | translate }}"><i class="fa fa-pencil"></i></a>' +
      '<a class="btn btn-warning" data-is-authorized-action="ROLE_ADMIN" data-ng-show="item.enable" data-table-body-has-action="enable-disable" data-ng-click="disable(item)" title="{{ \'GLOBAL.DISABLE\' | translate }}"><i class="fa fa-ban"></i></a>' +
      '<a class="btn btn-success" data-is-authorized-action="ROLE_ADMIN" data-ng-hide="item.enable" data-table-body-has-action="enable-disable" data-ng-click="enable(item)" title="{{ \'GLOBAL.ENABLE\' | translate }}"><i class="fa fa fa-check"></i></a>' +
      '<a class="btn btn-danger" data-is-authorized-action="ROLE_ADMIN" data-table-body-has-action="remove" data-table-body-action-remove-item="{{ item }}" title="{{ \'GLOBAL.REMOVE\' | translate }}"><i class="fa fa-trash-o"></i></a>' +
      '</div>' +
      '<div class="btn-group-vertical visible-xs">' +
      '<a data-ng-href="#/{{ module }}/show/{{ item.id }}" data-is-authorized-action="ROLE_ADMIN" data-table-body-has-action="show" class="btn btn-primary" title="{{ \'GLOBAL.SHOW\' | translate }}"><i class="fa fa-search-plus"></i></a>' +
      '<a data-ng-href="#/{{ module }}/edit/{{ item[\'id\'] }}" data-is-authorized-action="ROLE_ADMIN" data-table-body-has-action="edit" data-table-body-action-edit-item="{{ item }}" class="btn btn-primary" title="{{ \'GLOBAL.EDIT\' | translate }}"><i class="fa fa-pencil"></i></a>' +
      '<a class="btn btn-warning" data-is-authorized-action="ROLE_ADMIN" data-ng-show="item.enable" data-table-body-has-action="enable-disable" data-ng-click="disable(item)" title="{{ \'GLOBAL.DISABLE\' | translate }}"><i class="fa fa-ban"></i></a>' +
      '<a class="btn btn-success" data-is-authorized-action="ROLE_ADMIN" data-ng-hide="item.enable" data-table-body-has-action="enable-disable" data-ng-click="enable(item)" title="{{ \'GLOBAL.ENABLE\' | translate }}"><i class="fa fa fa-check"></i></a>' +
      '<a class="btn btn-danger" data-is-authorized-action="ROLE_ADMIN" data-table-body-has-action="remove" data-table-body-action-remove-item="{{ item }}" title="{{ \'GLOBAL.REMOVE\' | translate }}"><i class="fa fa-trash-o"></i></a>' +
      '</div>'
    );
  }]);

  angular.module('angular-tera-directives/miscellaneous/footer-directive.html', []).run(['$templateCache', function($templateCache) {
    $templateCache.put('angular-tera-directives/miscellaneous/footer-directive.html',
      '<div class="pull-right hidden-xs">' +
      '<span data-ng-show="version" style="vertical-align: middle;"><b>Version</b> {{ version }}</span>' +
      '</div>' +
      '<a href="http://www.teraswap.com" target="_blank"><img data-ng-src="assets/img/powered-{{ color }}.png" style="width:7em;" data-ng-hide="noLogo" /></a>'
    );
  }]);

  angular.module('angular-tera-directives', ['angular-tera-directives/box/box-filter-directive.html', 'angular-tera-directives/box/box-header-directive.html', 'angular-tera-directives/table/table-head-directive.html', 'angular-tera-directives/table/table-body-directive.html', 'angular-tera-directives/table/table-pager-directive.html', 'angular-tera-directives/table/table-actions-directive.html', 'angular-tera-directives/miscellaneous/footer-directive.html'])
    // box directives
    .directive('boxFilter', boxFilter)
    .directive('boxHeader', boxHeader)
    .directive('boxHeaderEvaluateAction', boxHeaderEvaluateAction)
    .directive('boxSimpleCollection', boxSimpleCollection)
    .directive('boxNotBooleanShow', boxNotBooleanShow)
    .directive('isAnAddAction', isAnAddAction)
    // table directives
    .directive('tableHead', tableHead)
    .directive('tableSortField', tableSortField)
    .directive('tableBody', tableBody)
    .directive('tableBodyValue', tableBodyValue)
    .directive('tableBodyActionEditItem', tableBodyActionEditItem)
    .directive('tableBodyActionRemoveItem', tableBodyActionRemoveItem)
    .directive('tableBodyHasAction', tableBodyHasAction)
    .directive('tableBodyActions', tableBodyActions)
    .directive('tablePager', tablePager)
    .directive('isAuthorizedAction', isAuthorizedAction)
    // miscellaneous
    .directive('mainFooter', mainFooter);
})();
